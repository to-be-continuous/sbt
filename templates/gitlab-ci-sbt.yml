# =========================================================================================
# Copyright (C) 2021 Orange & contributors
#
# This program is free software; you can redistribute it and/or modify it under the terms 
# of the GNU Lesser General Public License as published by the Free Software Foundation; 
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along with this 
# program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth 
# Floor, Boston, MA  02110-1301, USA.
# =========================================================================================
# default workflow rules: Merge Request pipelines
spec:
  inputs:
    image:
      description: The Docker image used to run sbt - **set the version required by your project**
      default: registry.hub.docker.com/sbtscala/scala-sbt:17.0.2_1.6.2_3.1.3
    build-args:
      description: The sbt arguments for the sbt arguments for the [build job packaging](https://www.scala-sbt.org/1.x/docs/Running.html#Common+commands)
      default: clean package
    test-args:
      description: The sbt arguments for the sbt arguments for the [build job test phase](https://www.scala-sbt.org/1.x/docs/Running.html#Common+commands)
      default: coverage test coverageAggregate
    opts:
      description: Global [sbt options](https://www.scala-sbt.org/1.x/docs/Command-Line-Reference.html#sbt+JVM+options+and+system+properties)
      default: -Dsbt.global.base=sbt-cache/sbtboot -Dsbt.boot.directory=sbt-cache/boot -Dsbt.coursier.home=sbt-cache/coursier -Dsbt.ci=true -Dsbt.color=always
    cli-opts:
      description: Additional sbt options used on the command line
      default: --batch
    sbom-disabled:
      description: Disable Software Bill of Materials
      type: boolean
      default: false
    sbom-image:
      description: The syft image used for SBOM analysis
      default: registry.hub.docker.com/anchore/syft:debug
    sbom-opts:
      description: Options for syft used for SBOM analysis
      default: dir:sbt-cache/coursier --catalogers java-cataloger
    publish-mode:
      description: Publish mode (one of _none_, `snapshot`, `ontag`, `release`)
      options:
      - ''
      - snapshot
      - ontag
      - release
      default: ''
    maven-repository-host:
      description: Global Maven repository host where credentials apply
      default: ''
    maven-repository-publish-release-url:
      description: Maven repository URL where to publish release artifacts
      default: ''
    maven-repository-publish-snapshot-url:
      description: Maven repository URL where to publish snapshot artifacts
      default: ''
---
workflow:
  rules:
    # prevent MR pipeline originating from production or integration branch(es)
    - if: '$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ $PROD_REF || $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ $INTEG_REF'
      when: never
    # on non-prod, non-integration branches: prefer MR pipeline over branch pipeline
    - if: '$CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS && $CI_COMMIT_REF_NAME !~ $PROD_REF && $CI_COMMIT_REF_NAME !~ $INTEG_REF'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*tag(,[^],]*)*\]/" && $CI_COMMIT_TAG'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*branch(,[^],]*)*\]/" && $CI_COMMIT_BRANCH'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*mr(,[^],]*)*\]/" && $CI_MERGE_REQUEST_ID'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*default(,[^],]*)*\]/" && $CI_COMMIT_REF_NAME =~ $CI_DEFAULT_BRANCH'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*prod(,[^],]*)*\]/" && $CI_COMMIT_REF_NAME =~ $PROD_REF'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*integ(,[^],]*)*\]/" && $CI_COMMIT_REF_NAME =~ $INTEG_REF'
      when: never
    - if: '$CI_COMMIT_MESSAGE =~ "/\[(ci skip|skip ci) on ([^],]*,)*dev(,[^],]*)*\]/" && $CI_COMMIT_REF_NAME !~ $PROD_REF && $CI_COMMIT_REF_NAME !~ $INTEG_REF'
      when: never
    - when: always

# test job prototype: implement adaptive pipeline rules
.test-policy:
  rules:
    # on tag: auto & failing
    - if: $CI_COMMIT_TAG
    # on ADAPTIVE_PIPELINE_DISABLED: auto & failing
    - if: '$ADAPTIVE_PIPELINE_DISABLED == "true"'
    # on production or integration branch(es): auto & failing
    - if: '$CI_COMMIT_REF_NAME =~ $PROD_REF || $CI_COMMIT_REF_NAME =~ $INTEG_REF'
    # early stage (dev branch, no MR): manual & non-failing
    - if: '$CI_MERGE_REQUEST_ID == null && $CI_OPEN_MERGE_REQUESTS == null'
      when: manual
      allow_failure: true
    # Draft MR: auto & non-failing
    - if: '$CI_MERGE_REQUEST_TITLE =~ /^Draft:.*/'
      allow_failure: true
    # else (Ready MR): auto & failing
    - when: on_success

# software delivery job prototype: run on production and integration branches + release pipelines
.delivery-policy:
  rules:
    # on tag with release pattern
    - if: '$CI_COMMIT_TAG =~ $RELEASE_REF'
    # on production or integration branch(es)
    - if: '$CI_COMMIT_REF_NAME =~ $PROD_REF || $CI_COMMIT_REF_NAME =~ $INTEG_REF'

variables:
  # Global TBC SBOM Mode (onrelease -> only generate SBOMs for releases, always -> generate SBOMs for all refs)
  TBC_SBOM_MODE: "onrelease"

  # SBT image (can be overridden)
  SBT_IMAGE: $[[ inputs.image ]]

  SBT_TEST_ARGS: $[[ inputs.test-args ]]
  SBT_BUILD_ARGS: $[[ inputs.build-args ]]

  # sets cache dirs
  SBT_OPTS: $[[ inputs.opts ]]

  SBT_CLI_OPTS: $[[ inputs.cli-opts ]]

  SBT_SBOM_IMAGE: $[[ inputs.sbom-image ]]
  SBT_SBOM_OPTS: $[[ inputs.sbom-opts ]]

  SBT_SBOM_DISABLED: $[[ inputs.sbom-disabled ]]
  SBT_PUBLISH_MODE: $[[ inputs.publish-mode ]]
  MAVEN_REPOSITORY_HOST: $[[ inputs.maven-repository-host ]]
  MAVEN_REPOSITORY_PUBLISH_RELEASE_URL: $[[ inputs.maven-repository-publish-release-url ]]
  MAVEN_REPOSITORY_PUBLISH_SNAPSHOT_URL: $[[ inputs.maven-repository-publish-snapshot-url ]]

  # default production ref name (pattern)
  PROD_REF: /^(master|main)$/
  # default integration ref name (pattern)
  INTEG_REF: /^develop$/
  # default release tag name (pattern)
  RELEASE_REF: /^v?[0-9]+\.[0-9]+\.[0-9]+(-[a-zA-Z0-9-\.]+)?(\+[a-zA-Z0-9-\.]+)?$/

stages:
  - build
  - test
  - package-build
  - package-test
  - infra
  - deploy
  - acceptance
  - publish
  - infra-prod
  - production

.sbt-scripts: &sbt-scripts |
  # BEGSCRIPT
  set -e

  function log_info() {
      echo -e "[\\e[1;94mINFO\\e[0m] $*"
  }

  function log_warn() {
      echo -e "[\\e[1;93mWARN\\e[0m] $*"
  }

  function log_error() {
      echo -e "[\\e[1;91mERROR\\e[0m] $*"
  }

  function assert_defined() {
    if [[ -z "$1" ]]
    then
      log_error "$2"
      exit 1
    fi
  }

  function install_ca_certs() {
    certs=$1
    if [[ -z "$certs" ]]
    then
      return
    fi

    # import in system
    if echo "$certs" >> /etc/ssl/certs/ca-certificates.crt
    then
      log_info "CA certificates imported in \\e[33;1m/etc/ssl/certs/ca-certificates.crt\\e[0m"
    fi
    if echo "$certs" >> /etc/ssl/cert.pem
    then
      log_info "CA certificates imported in \\e[33;1m/etc/ssl/cert.pem\\e[0m"
    fi

    # import in Java keystore (if keytool command found)
    if command -v keytool > /dev/null
    then
      # shellcheck disable=SC2046
      javahome=${JAVA_HOME:-$(dirname $(readlink -f $(command -v java)))/..}
      # shellcheck disable=SC2086
      keystore=${JAVA_KEYSTORE_PATH:-$(ls -1 $javahome/jre/lib/security/cacerts 2>/dev/null || ls -1 $javahome/lib/security/cacerts 2>/dev/null || echo "")}
      if [[ -f "$keystore" ]]
      then
        storepass=${JAVA_KEYSTORE_PASSWORD:-changeit}
        nb_certs=$(echo "$certs" | grep -c 'END CERTIFICATE')
        log_info "importing $nb_certs certificates in Java keystore \\e[33;1m$keystore\\e[0m..."
        for idx in $(seq 0 $((nb_certs - 1)))
        do
          # TODO: use keytool option -trustcacerts ?
          if echo "$certs" | awk "n==$idx { print }; /END CERTIFICATE/ { n++ }" | keytool -noprompt -import -alias "imported CA Cert $idx" -keystore "$keystore" -storepass "$storepass"
          then
            log_info "... CA certificate [$idx] successfully imported"
          else
            log_warn "... Failed importing CA certificate [$idx]: abort"
            return
          fi
        done
      else
        log_warn "Java keystore \\e[33;1m$keystore\\e[0m not found: could not import CA certificates"
      fi
    fi
  }

  function unscope_variables() {
    _scoped_vars=$(env | awk -F '=' "/^scoped__[a-zA-Z0-9_]+=/ {print \$1}" | sort)
    if [[ -z "$_scoped_vars" ]]; then return; fi
    log_info "Processing scoped variables..."
    for _scoped_var in $_scoped_vars
    do
      _fields=${_scoped_var//__/:}
      _condition=$(echo "$_fields" | cut -d: -f3)
      case "$_condition" in
      if) _not="";;
      ifnot) _not=1;;
      *)
        log_warn "... unrecognized condition \\e[1;91m$_condition\\e[0m in \\e[33;1m${_scoped_var}\\e[0m"
        continue
      ;;
      esac
      _target_var=$(echo "$_fields" | cut -d: -f2)
      _cond_var=$(echo "$_fields" | cut -d: -f4)
      _cond_val=$(eval echo "\$${_cond_var}")
      _test_op=$(echo "$_fields" | cut -d: -f5)
      case "$_test_op" in
      defined)
        if [[ -z "$_not" ]] && [[ -z "$_cond_val" ]]; then continue; 
        elif [[ "$_not" ]] && [[ "$_cond_val" ]]; then continue; 
        fi
        ;;
      equals|startswith|endswith|contains|in|equals_ic|startswith_ic|endswith_ic|contains_ic|in_ic)
        # comparison operator
        # sluggify actual value
        _cond_val=$(echo "$_cond_val" | tr '[:punct:]' '_')
        # retrieve comparison value
        _cmp_val_prefix="scoped__${_target_var}__${_condition}__${_cond_var}__${_test_op}__"
        _cmp_val=${_scoped_var#"$_cmp_val_prefix"}
        # manage 'ignore case'
        if [[ "$_test_op" == *_ic ]]
        then
          # lowercase everything
          _cond_val=$(echo "$_cond_val" | tr '[:upper:]' '[:lower:]')
          _cmp_val=$(echo "$_cmp_val" | tr '[:upper:]' '[:lower:]')
        fi
        case "$_test_op" in
        equals*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != "$_cmp_val" ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == "$_cmp_val" ]]; then continue; 
          fi
          ;;
        startswith*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != "$_cmp_val"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == "$_cmp_val"* ]]; then continue; 
          fi
          ;;
        endswith*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != *"$_cmp_val" ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == *"$_cmp_val" ]]; then continue; 
          fi
          ;;
        contains*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != *"$_cmp_val"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == *"$_cmp_val"* ]]; then continue; 
          fi
          ;;
        in*)
          if [[ -z "$_not" ]] && [[ "__${_cmp_val}__" != *"__${_cond_val}__"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "__${_cmp_val}__" == *"__${_cond_val}__"* ]]; then continue; 
          fi
          ;;
        esac
        ;;
      *)
        log_warn "... unrecognized test operator \\e[1;91m${_test_op}\\e[0m in \\e[33;1m${_scoped_var}\\e[0m"
        continue
        ;;
      esac
      # matches
      _val=$(eval echo "\$${_target_var}")
      log_info "... apply \\e[32m${_target_var}\\e[0m from \\e[32m\$${_scoped_var}\\e[0m${_val:+ (\\e[33;1moverwrite\\e[0m)}"
      _val=$(eval echo "\$${_scoped_var}")
      export "${_target_var}"="${_val}"
    done
    log_info "... done"
  }

  # evaluate and export a secret
  # - $1: secret variable name
  function eval_secret() {
    name=$1
    value=$(eval echo "\$${name}")
    case "$value" in
    @b64@*)
      decoded=$(mktemp)
      errors=$(mktemp)
      if echo "$value" | cut -c6- | base64 -d > "${decoded}" 2> "${errors}"
      then
        # shellcheck disable=SC2086
        export ${name}="$(cat ${decoded})"
        log_info "Successfully decoded base64 secret \\e[33;1m${name}\\e[0m"
      else
        fail "Failed decoding base64 secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
      fi
      ;;
    @hex@*)
      decoded=$(mktemp)
      errors=$(mktemp)
      if echo "$value" | cut -c6- | sed 's/\([0-9A-F]\{2\}\)/\\\\x\1/gI' | xargs printf > "${decoded}" 2> "${errors}"
      then
        # shellcheck disable=SC2086
        export ${name}="$(cat ${decoded})"
        log_info "Successfully decoded hexadecimal secret \\e[33;1m${name}\\e[0m"
      else
        fail "Failed decoding hexadecimal secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
      fi
      ;;
    @url@*)
      url=$(echo "$value" | cut -c6-)
      if command -v curl > /dev/null
      then
        decoded=$(mktemp)
        errors=$(mktemp)
        if curl -s -S -f --connect-timeout 5 -o "${decoded}" "$url" 2> "${errors}"
        then
          # shellcheck disable=SC2086
          export ${name}="$(cat ${decoded})"
          log_info "Successfully curl'd secret \\e[33;1m${name}\\e[0m"
        else
          log_warn "Failed getting secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
        fi
      elif command -v wget > /dev/null
      then
        decoded=$(mktemp)
        errors=$(mktemp)
        if wget -T 5 -O "${decoded}" "$url" 2> "${errors}"
        then
          # shellcheck disable=SC2086
          export ${name}="$(cat ${decoded})"
          log_info "Successfully wget'd secret \\e[33;1m${name}\\e[0m"
        else
          log_warn "Failed getting secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
        fi
      else
        fail "Couldn't get secret \\e[33;1m${name}\\e[0m: no http client found"
      fi
      ;;
    esac
  }

  function eval_all_secrets() {
    encoded_vars=$(env | grep -v '^scoped__' | awk -F '=' '/^[a-zA-Z0-9_]*=@(b64|hex|url)@/ {print $1}')
    for var in $encoded_vars
    do
      eval_secret "$var"
    done
  }

  # builds the Java proxy options from Linux env (http_proxy, https_proxy, ftp_proxy and no_proxy)
  function eval_java_proxy_args() {
    # transform no_proxy into Java stype nonProxyHosts
    nph=$(echo "${no_proxy:-$NO_PROXY}" | sed -e 's/\s*//g' -e 's/^\./*./' -e 's/,\./,*./g' -e 's/,/|/g')
    java_proxy_args="$(java_proto_proxy_args http "${http_proxy:-$HTTP_PROXY}" "$nph") $(java_proto_proxy_args https "${https_proxy:-$HTTPS_PROXY}" "$nph") $(java_proto_proxy_args ftp "${ftp_proxy:-$FTP_PROXY}" "$nph")"
    export java_proxy_args
    if [[ "$java_proxy_args" ]]
    then
      log_info "Using Java proxy options (from env):  \\e[33;1m$java_proxy_args\\e[0m"
    fi
  }

  function java_proto_proxy_args() {
    proto=$1
    proxy=$2
    non_proxy_hosts=$3
    if [[ "$proxy" ]]
    then
      host_port=$(echo "$proxy" | cut -d'/' -f3)
      host=$(echo "$host_port" | cut -d':' -f1)
      port=$(echo "$host_port" | cut -s -d':' -f2)
      proto_proxy_args="-D$proto.proxyHost=$host -D$proto.proxyPort=${port:-80}"
      if [[ "$non_proxy_hosts" ]]; then proto_proxy_args="$proto_proxy_args -D$proto.nonProxyHosts=\"$non_proxy_hosts\""; fi
      echo "$proto_proxy_args"
    fi
  }

  unscope_variables
  eval_all_secrets

  # ENDSCRIPT

# Generic SBT job
.sbt-base:
  image: $SBT_IMAGE
  services:
    - name: "$TBC_TRACKING_IMAGE"
      command: ["--service", "sbt", "1.8.1"]
  before_script:
    - !reference [.sbt-scripts]
    - install_ca_certs "${CUSTOM_CA_CERTS:-$DEFAULT_CA_CERTS}"
    - eval_java_proxy_args
  # Cache downloaded dependencies and plugins between builds.
  cache:
    key: "$CI_COMMIT_REF_SLUG-sbt"
    paths:
      - sbt-cache/
      - project/target/
      - project/project/

sbt-build:
  extends: .sbt-base
  stage: build
  script:
    - sbt ${TRACE+-v} $java_proxy_args $SBT_TEST_ARGS
    - tar cvf /tmp/reports-backup.tar $(find . -type d \( -name "*-reports" -o -name "coverage-report" \) ) || true # to not fail when nothing is found
    - sbt ${TRACE+-v} $java_proxy_args $SBT_BUILD_ARGS
    - tar xvf /tmp/reports-backup.tar || true # to not fail when nothing has been found
  # code coverage RegEx
  coverage: '/Statement coverage[A-Za-z\.*]\s*:\s*([^%]+)/'
  # keep build artifacts and JUnit reports
  artifacts:
    name: "$CI_JOB_NAME artifacts from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    expire_in: 1 day
    paths:
      - "**/target"
    reports:
      junit:
        - "**/target/*-reports/*.xml"
      coverage_report:
        coverage_format: cobertura
        path : "**/target/scala-*/coverage-report/cobertura.xml"

sbt-sbom:
  extends: .sbt-base
  stage: test
  image:
    name: $SBT_SBOM_IMAGE
    entrypoint: [""]
  script:
    - mkdir -p -m 777 reports
    - /syft scan $SBT_SBOM_OPTS -o cyclonedx-json=reports/sbt-sbom.cyclonedx.json
    - chmod a+r reports/sbt-sbom.cyclonedx.json
  artifacts:
    name: "SBOM for sbt from $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    when: always
    expire_in: 1 week
    paths:
      - reports/sbt-sbom.cyclonedx.json
    reports:
      cyclonedx: 
        - reports/sbt-sbom.cyclonedx.json
  rules:
    # exclude if disabled
    - if: '$SBT_SBOM_DISABLED == "true"'
      when: never
    # 'always' mode: run
    - if: '$TBC_SBOM_MODE == "always"'
    # exclude unsupported modes
    - if: '$TBC_SBOM_MODE != "onrelease"'
      when: never
    # 'onrelease' mode: use common software delivery rules
    - !reference [.delivery-policy, rules]

sbt-publish-snapshot:
  extends: .sbt-base
  stage: publish
  script:
    - sbt $SBT_CLI_OPTS ${TRACE+-v} $java_proxy_args publish
  rules:
    # on tags: never
    - if: $CI_COMMIT_TAG
      when: never
    # else: if $SBT_PUBLISH_MODE is set
    - if: $SBT_PUBLISH_MODE

sbt-publish-stable:
  extends: .sbt-base
  stage: publish
  script:
    - if [[ "$CI_COMMIT_TAG" =~ ([0-9]+\.[0-9]+\.[0-9]+) ]]; then tag_version="${BASH_REMATCH[1]}"; fi
    - assert_defined "$tag_version" "Couldn't determine the version from the release tag ($CI_COMMIT_TAG)"
    - sbt $SBT_CLI_OPTS ${TRACE+-v} $java_proxy_args "set version:=\"$tag_version\"" publish
  rules:
    # exclude if $SBT_PUBLISH_MODE not 'ontag' or 'release'
    - if: '$SBT_PUBLISH_MODE != "ontag" && $SBT_PUBLISH_MODE != "release"'
      when: never
    # on tag with release pattern: auto
    - if: '$CI_COMMIT_TAG =~ $RELEASE_REF'

sbt-release:
  extends: .sbt-base
  stage: publish
  dependencies: []
  before_script:
    - !reference [.sbt-scripts]
    - install_ca_certs "${CUSTOM_CA_CERTS:-$DEFAULT_CA_CERTS}"
    - eval_java_proxy_args
    # configure GIT repository for write operations using a DEDICATED SSH PRIVATE KEY
    - assert_defined "$GIT_PRIVATE_KEY" 'Missing required env $GIT_PRIVATE_KEY'
    - eval $(ssh-agent -s)
    - echo "$GIT_PRIVATE_KEY" | ssh-add -
    - mkdir -m 700 $HOME/.ssh
    - ssh-keyscan -H $CI_SERVER_HOST >> ~/.ssh/known_hosts
    - log_info "--- Fixing Git setup for branch $CI_COMMIT_REF_NAME"
    - git reset --hard
    - git clean -fd
    - git checkout ${CI_COMMIT_REF_NAME}
    - git pull origin ${CI_COMMIT_REF_NAME}
    - git config user.name $GITLAB_USER_ID
    - git config user.email $GITLAB_USER_EMAIL
    - log_info "--- Switching to a SSH GIT access to allow write access through private key"
    - git remote set-url origin "git@${CI_SERVER_HOST}:$(echo $CI_REPOSITORY_URL | cut -d'/' -f4-)"
  script:
    sbt $SBT_CLI_OPTS ${TRACE+-v} $java_proxy_args ';
      set releaseIgnoreUntrackedFiles:=true;
      release with-defaults'
  rules:
    # exclude if $SBT_PUBLISH_MODE not 'release'
    - if: '$SBT_PUBLISH_MODE != "release"'
      when: never
    # on production or integration branch: manual, non blocking
    - if: '$CI_COMMIT_REF_NAME =~ $PROD_REF || $CI_COMMIT_REF_NAME =~ $INTEG_REF'
      when: manual
      allow_failure: true
